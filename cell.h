#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <termios.h>

// A structure to hold the parts of a string,
// space splitted
typedef struct{
    char **parts;                   // The parts of the string
    size_t part_count;              // Number of parts
} CellStringParts;

// Utility method to `free` a `CellStringParts` structure
void cell_stringparts_free(CellStringParts parts);

// Forward declaration of the structure
struct Cell;

// Callback to invoke when a keyword is entered
// parts    : The line when the keyword is entered, space splitted
// cell     : The state of the Cell when the callback is invoked
typedef void (*cell_action)(const CellStringParts parts, struct Cell *cell);

// An enum to denote one of the six ANSI_COLOR_*
typedef enum{
    CELL_COLOR_RED = 0,
    CELL_COLOR_GREEN = 1,
    CELL_COLOR_YELLOW = 2,
    CELL_COLOR_BLUE = 3,
    CELL_COLOR_MAGENTA = 4,
    CELL_COLOR_CYAN = 5
} CellColor;

// A structure to represent a keyword of the Cell
typedef struct CellKeyword{
    char *keyword;                      // Name of the keyword
    const char *shorthelp;              // A short oneliner help message about the keyword
    const char *longhelp;               // A long descriptive help message about the keyword
    CellColor color;                    // Highlight color of the keyword when entered
    cell_action action;                 // Callback to invoke when the keyword is entered
    size_t length;                      // Length of the keyword string         (populated automatically)
    size_t subcommand_count;            // Number of subcommands of the keyword (populated automatically)
    struct CellKeyword *subcommands;    // Array of subcommands of the keyword  (populated automatically)
} CellKeyword;

// The state of the REPL itself
typedef struct Cell{
    tcflag_t c_lflag_bak;               // Terminal properties before the REPL starts
    char *line, c;                      // Currently entered line and character
    char *prefix;                       // The prefix to display to the left side of the REPL
    char *tabline;                      // State of line when autocompletion is triggered
    char **history;                     // Collection of all strings entered
    size_t size;                        // Size of the present line
    size_t tabsize;                     // Size of the line when autocompletion is triggered
    size_t curpos;                      // Current position of the cursor in the line
    size_t tabpos;                      // Position when autocompletion is triggered
    size_t allocated_size;              // Currently allocated length of `line`
    size_t history_size;                // Size of the history array
    size_t history_pointer;             // A pointer to move up or down on consecutive keypress
    size_t keyword_count;               // Number of keywords in the Cell
    uint8_t escseen;                    // Flag to denote whether an ESC keypress is seen
    uint8_t run;                        // Flag to denote whether the REPL should be continued
    uint8_t tabcount;                   // Counts the number of consecutive tabs
    CellKeyword *keywords;              // Array of keywords in the Cell
} Cell;

// ANSI Colors
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_FONT_BOLD     "\x1b[1m"

// Initialize a Cell
Cell cell_init(const char *prefix);
// Create a keyword with the given name, oneliner help and callback
CellKeyword cell_create_keyword(const char *keyword, const char *help, cell_action action);
// Add a subkeyword to a previously created keyword
void cell_add_subkeyword(CellKeyword *keyword, CellKeyword subkeyword);
// Insert a keyword to a Cell
void cell_insert_keyword(Cell *t, CellKeyword keyword);
// Create and insert a keyword directly to a Cell
void cell_add_keyword(Cell *t, const char *keyword, const char *help, cell_action action);
// Start the REPL for the given Cell
void cell_repl(Cell *t);
// Destroy the given Cell
void cell_destroy(Cell *t);

// Default 'help' callback, prints targetted helps and lists
// all registered keywords as required
void cell_default_help(const CellStringParts csp, Cell *c);
// Default callback for a keyword which is not yet implemented
void cell_default_not_implemented(const CellStringParts csp, Cell *c);

// Cosmetic functions to print various types of colored messages
// to the user

// The following functions should be used to print various
// levels to info to the terminal. The output would look
// something like this :
// [Info] This is a information
// where [Info] is bolded and colored blue
//
// When CELL_DEBUG flag is macro is defined, these functions will also
// print the file name, line number and calling function
// from where they are invoked

#ifdef CELL_DEBUG
#define cell_pdbg(x, ...) cell_ddbg( ANSI_FONT_BOLD "<%s:%d:%s> " ANSI_COLOR_RESET x, __FILE__, __LINE__, __func__, ##__VA_ARGS__)
void cell_ddbg(const char *msg, ...);

#define cell_perr(x, ...) cell_derr( ANSI_FONT_BOLD "<%s:%d:%s> " ANSI_COLOR_RESET x, __FILE__, __LINE__, __func__, ##__VA_ARGS__)
void cell_derr(const char *msg, ...);

#define cell_pinfo(x, ...) cell_dinfo( ANSI_FONT_BOLD "<%s:%d:%s> " ANSI_COLOR_RESET x, __FILE__, __LINE__, __func__, ##__VA_ARGS__)
void cell_dinfo(const char *msg, ...);

#define cell_pwarn(x, ...) cell_dwarn( ANSI_FONT_BOLD "<%s:%d:%s> " ANSI_COLOR_RESET x, __FILE__, __LINE__, __func__, ##__VA_ARGS__)
void cell_dwarn(const char *msg, ...);
#else
// [Debug] This is a debug message
void cell_pdbg(const char *msg, ...);
// [Error] You did something wrong
void cell_perr(const char *msg, ...);
// [Info] This is an information
void cell_pinfo(const char *msg, ...);
// [Warning] Something horrible is about to happen
void cell_pwarn(const char *msg, ...);
#endif

// Print various colored texts in usual
// printf format, without using the ANSI_COLOR_*
// macros
void cell_pred(const char *msg, ...);
void cell_pblue(const char *msg, ...);
void cell_pgrn(const char *msg, ...);
void cell_pylw(const char *msg, ...);
void cell_pcyn(const char *msg, ...);
void cell_pmgn(const char *msg, ...);
