#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <stdarg.h>

#include "cell.h"

typedef size_t siz;
#define Psiz "zd"
typedef uint8_t u8;

#define KB_BACKSPACE 127
#define KB_DELETE 126
#define KB_TAB 9
#define KB_ARW_UP 65
#define KB_ARW_DWN 66
#define KB_ARW_RGT 67
#define KB_ARW_LFT 68
#define KB_ESC 27
#define KB_SQB_OPN 91

void cell_stringparts_free(CellStringParts s){
    for(siz i = 0;i < s.part_count;i++)
        free(s.parts[i]);
    free(s.parts);
}

static const char* colors[] = {
    ANSI_COLOR_RED,
    ANSI_COLOR_GREEN,
    ANSI_COLOR_YELLOW,
    ANSI_COLOR_BLUE,
    ANSI_COLOR_MAGENTA,
    ANSI_COLOR_CYAN
};

static CellStringParts cell_string_split(const char *str, char c, siz max){
    CellStringParts sp;
    sp.part_count = 0;
    sp.parts = NULL;
    siz s = 0;
    while(str[s] != '\0' && s < max){
        sp.parts = (char **)realloc(sp.parts, sizeof(char *) * ++sp.part_count);
        sp.parts[sp.part_count - 1] = NULL;
        siz t = 0;
        while(str[s] != '\0' && str[s] != c && s < max){
            sp.parts[sp.part_count - 1] = (char *)realloc(sp.parts[sp.part_count - 1],
                    ++t);
            sp.parts[sp.part_count - 1][t - 1] = str[s];
            s++;
        }
        if(str[s] == c)
            s++;
        sp.parts[sp.part_count - 1] = (char *)realloc(sp.parts[sp.part_count - 1], ++t);
        sp.parts[sp.part_count - 1][t - 1] = '\0';
    }
    return sp;
}

static void cell_noecho(Cell *term){
    struct termios t;
    tcgetattr(0, &t);
    term->c_lflag_bak = t.c_lflag;
    t.c_lflag &= ~(ECHO|ICANON);
    tcsetattr(0, TCSANOW, &t);
}

static void cell_noecho_off(Cell *term){
    struct termios t;
    tcgetattr(0, &t);
    t.c_lflag = term->c_lflag_bak;
    tcsetattr(0, TCSANOW, &t);
}

static siz cell_keyword_get_pos(const char *keyword, CellKeyword *keywords, siz keyword_count){
    siz j = 0,i = 0, bound = keyword_count;
    while(keyword[j] != '\0' && i < bound){
        if(keywords[i].length < j+1){
            i++;
            if(i == bound)
                break;
        }
        if(keywords[i].keyword[j] < keyword[j]){
            i++;
        }
        else if(keywords[i].keyword[j] == keyword[j]){
            bound = i;
            while(bound < keyword_count &&
                    keywords[bound].length > j &&
                    keywords[bound].keyword[j] == keyword[j])
                bound++;
            j++;
        }
        else
            break;
    }
    return i;
}

static CellKeyword* cell_keyword_match(const char* line, CellKeyword *keywords, siz keyword_count){
    //CellStringParts sp = cell_string_split(&line[pos], ' ', strlen(line));
    //dbg("Searching %s\n", sp.parts[0]);
    siz pos = cell_keyword_get_pos(line, keywords, keyword_count);
    if(pos >= keyword_count)
        return NULL;
    if(strcmp(line, keywords[pos].keyword) == 0)
        return &keywords[pos];
    return NULL;
}

static void cell_highlight_and_print(Cell *t, siz max){
    printf("\r%s ", t->prefix);
    siz printed = 0, colored = 0;
    CellStringParts part = cell_string_split(t->line, ' ', strlen(t->line));
    for(siz i = 0;i < part.part_count && printed < max;i++){
        //u8 space_printed = 0;
        if(part.parts[i][0] != '\0'){
            if(!colored){
                CellKeyword* keyword = cell_keyword_match(part.parts[i], t->keywords, t->keyword_count);
                if(keyword != NULL){
                    printf("%s", colors[keyword->color]);
                }
                else
                    printf(ANSI_COLOR_RED);
                colored = 1;
            }
            for(siz j = 0;printed<max && part.parts[i][j] != '\0';j++,printed++)
                putchar(part.parts[i][j]);
            printf(ANSI_COLOR_RESET);
        }
        if(printed < max)
            putchar(' '), printed++;
    }
    cell_stringparts_free(part);
}

static void cell_line_clear(const char *prefix, siz size){
    printf("\r%*s ", (int)strlen(prefix), " ");
    for(siz i = 0;i < size + 1;i++)
        printf(" ");
}

static void cell_execute_statement(Cell *t){
    CellStringParts sp = cell_string_split(t->line, ' ', t->size);
    siz partnum = 0, prev = 0, ki = 0;
    u8 hasone = 0;
    CellKeyword *keywords = t->keywords;
    CellKeyword *last_match = NULL;
    siz keyword_count = t->keyword_count;
    while(partnum < sp.part_count){
        if(sp.parts[partnum][0] != '\0'){
            hasone = 1;
            CellKeyword *ck = cell_keyword_match(sp.parts[partnum], keywords, keyword_count);
            if(ck != NULL){
                ki = partnum;
                last_match = ck;
            }
            if(ck != NULL && ck->subcommands != NULL){
                keywords = ck->subcommands;
                keyword_count = ck->subcommand_count;
            }
            else
                break;
            prev = partnum;
        }
        partnum++;
    }

    if(!hasone){
        cell_stringparts_free(sp);
        return;
    }

    CellStringParts cp;
    if(ki != 0){
        cp.part_count = sp.part_count - ki;
        cp.parts = (char **)malloc(sizeof(char *) * cp.part_count);
        for(siz i = ki;i < sp.part_count;i++){
            cp.parts[i - ki] = sp.parts[i];
        }
        for(siz i = 0;i < ki;i++)
            free(sp.parts[i]);
        free(sp.parts);
    }
    else
        cp = sp;

    if(last_match != NULL){
        last_match->action(cp, t);
        cell_stringparts_free(cp);
        return;
    }

    cell_perr("No such keyword : %s", cp.parts[0]);
    siz pos = cell_keyword_get_pos(cp.parts[0], keywords, keyword_count);
    if(pos < keyword_count){
        u8 choices_print = 0;
        siz len = strlen(cp.parts[0]);
        while(pos < keyword_count && strncmp(cp.parts[0], keywords[pos].keyword, len) == 0){
            if(!choices_print){
                cell_pinfo("Possible choices : %s", keywords[pos].keyword);
                choices_print = 1;
            }
            else
                printf(", %s", keywords[pos].keyword);
            pos++;
        }
    }
    cell_stringparts_free(cp);
}

static void cell_line_insert(Cell *t, char c, siz pos){
    t->c = c;
    t->curpos = pos;
    if(t->allocated_size == 0){ 
        t->line = (char *)realloc(t->line, 2);
        t->allocated_size = 2;
        t->line[1] = '\0';
    }
    else if(t->allocated_size < (t->size + 2)){
        t->allocated_size++;
        t->line = (char *)realloc(t->line, (t->allocated_size));
        t->line[t->allocated_size - 1] = '\0';
    }
    for(siz i = t->size;i > t->curpos;i--)
        t->line[i] = t->line[i - 1];
    t->line[t->curpos] = t->c;
    //dbg("Curpos : %" Psiz "\tSize : %" Psiz "\n", curpos + 1, size + 1);
    cell_highlight_and_print(t, t->size + 1);
    t->curpos++;
    t->size++;
    if(t->curpos != t->size)
        cell_highlight_and_print(t, t->curpos);

}

static void cell_line_insert_char(Cell *t, char c){
    cell_line_insert(t, c, t->curpos);
}

static void cell_line_insert_tab(Cell *t){ 
    cell_line_insert_char(t, ' ');
    cell_line_insert_char(t, ' ');
    cell_line_insert_char(t, ' ');
    cell_line_insert_char(t, ' ');
}

static void cell_line_print(Cell *t, const char *show){
    siz i;
    cell_line_clear(t->prefix, t->size);
    for(i = 0;show[i]!='\0';i++)
        t->line[i] = show[i];
    t->line[i] = '\0';
    t->curpos = i;
    t->size = i;
    cell_highlight_and_print(t, t->size);
    if(t->curpos != t->size)
        cell_highlight_and_print(t, t->curpos);
}

void cell_repl(Cell *t){
    cell_noecho(t);
    printf("\n%s ", t->prefix);
    //char *line = NULL, c;
    //siz size = 0, curpos = 0;
    //u8 escseen = 0;
    while(t->run){
        t->c = getchar();
        //dbg("KeyPressed : %d [%c]", c, c);
        if(t->c == '\n'){
            t->tabcount = 0;
            if(t->size != 0){
                t->line[t->size] = '\0';
                cell_highlight_and_print(t, t->size);
                // Add the line to the history
                t->history = (char **)realloc(t->history, sizeof(char*)*(++t->history_size));
                t->history[t->history_size - 1] = strdup(t->line);
                t->history_pointer = t->history_size;
                cell_execute_statement(t);
                memset(t->line, 0, t->size + 1);
                t->size = 0;
                t->curpos = 0;
            }
            if(t->run)
                printf("\n%s ", t->prefix);
        }
        else if(t->c == KB_BACKSPACE || t->c == KB_DELETE){
            t->tabcount = 0;
            if(t->curpos > 0){
                cell_line_clear(t->prefix, t->size);
                for(siz i = t->curpos-1; i < t->size;i++)
                    t->line[i] = t->line[i+1];
                t->line[t->size - 1] = '\0';
                t->size--;
                t->curpos--;
                cell_highlight_and_print(t, t->size);
                if(t->curpos != t->size){
                    cell_highlight_and_print(t, t->curpos);
                }
                //printf(" ");
            }
        }
        else if(t->c == KB_ESC){
            t->escseen = 1;
            t->tabcount = 0;
        }
        else if(t->c == KB_TAB){
            if(t->curpos == 0){
                cell_line_insert_tab(t);
                continue;
            }
            if(!t->tabcount){
                if(t->tabline != NULL)
                    free(t->tabline);
                t->tabline = strdup(t->line);
                t->tabpos = t->curpos;
                t->tabsize = t->size;
            }

            CellStringParts lp = cell_string_split(t->tabline, ' ', t->tabsize);
            if(lp.parts[lp.part_count - 1][0] == '\0'){
                cell_line_insert_tab(t);
                cell_stringparts_free(lp);
                continue;
            }

            // Find the total word width
            siz fww = 0, i = 0, prev = 0;
            u8 stop = 0;
            CellKeyword *keywords = t->keywords;
            siz keyword_count = t->keyword_count;
            for(;i < lp.part_count;i++){
                if(lp.parts[i][0] == '\0')
                    fww++;
                else{
                    if(i < (lp.part_count - 1)){
                        CellKeyword *key = cell_keyword_match(lp.parts[prev], keywords, keyword_count);
                        if(key == NULL || key->subcommands == NULL){
                            stop = 1;
                            break;
                        }
                        else{
                            keywords = key->subcommands;
                            keyword_count = key->subcommand_count;
                        }
                        fww++;
                    }
                    prev = i;
                    fww += strlen(lp.parts[i]);
                    // break;
                }
            }
            if(stop){
                cell_line_insert_tab(t);
                cell_stringparts_free(lp);
                continue;
            }
            i = prev;
            if(t->tabpos == fww){ // Find autocomplete
                siz pos = cell_keyword_get_pos(lp.parts[i], keywords, keyword_count) + t->tabcount;
                if(pos >= keyword_count){ 
                    cell_stringparts_free(lp);
                    continue;
                }
                u8 matches = 1, j = 0;
                for(j = 0;lp.parts[i][j]!= '\0';j++){
                    if(lp.parts[i][j] != keywords[pos].keyword[j]){
                        matches = 0;
                        break;
                    }
                }
                if(matches){

                    t->curpos = t->tabpos;
                    siz min = (t->tabsize < t->size) ? t->tabsize : t->size;
                    for(siz l = 0;l < min;l++){
                        t->line[l] = t->tabline[l];
                    }
                    siz bak = t->tabsize;
                    while(bak <= t->size)
                        t->line[bak++] = ' ';
                    t->line[t->size] = '\0';
                    cell_highlight_and_print(t, t->size);
                    t->size = t->tabsize;

                    siz missing = keywords[pos].length - j;
                    siz finalpos = t->curpos + missing;
                    while(t->curpos < finalpos){
                        cell_line_insert_char(t, keywords[pos].keyword[j++]);
                    }
                    t->tabcount++;
                }
            }
            else{
                cell_line_insert_tab(t);
            }
            cell_stringparts_free(lp);
        }
        else if(t->escseen && t->c == '['){
            t->tabcount = 0;
            t->c = getchar();
            if(t->c == KB_ARW_LFT){
                if(t->curpos > 0){
                    t->curpos--;
                    //dbg("Curpos : %" Psiz "\tSize : %" Psiz "\n", curpos, size);
                    cell_highlight_and_print(t, t->curpos);
                }
            } 
            else if(t->c == KB_ARW_RGT){
                if(t->curpos < t->size){
                    t->curpos++;
                    //dbg("Curpos : %" Psiz "\tSize : %" Psiz "\n", curpos, size);
                    cell_highlight_and_print(t, t->curpos);
                }
            } 
            else if(t->c == KB_ARW_UP){
                if(t->history_size == 0)
                    continue;
                if(t->history_pointer > 0)
                    t->history_pointer--;
                char *hist = t->history[t->history_pointer];
                cell_line_print(t, hist);
            }
            else if(t->c == KB_ARW_DWN){
                if(t->history_size == 0)
                    continue;
                if(t->history_pointer < t->history_size)
                    t->history_pointer++;
                if(t->history_pointer == t->history_size){
                    cell_line_clear(t->prefix, t->size);
                    t->size = t->curpos = 0;
                    cell_highlight_and_print(t, 0);
                    continue;
                }
                char *hist = t->history[t->history_pointer];
                cell_line_print(t, hist);
            }
        }
        else{
            t->tabcount = 0;
            cell_line_insert_char(t, t->c);
        }
    }
    free(t->line);
    t->line = NULL;
    t->size = 0;
    t->curpos = 0;
    cell_noecho_off(t);
}

Cell cell_init(const char *prefix){
    Cell t;
    t.c = 0;
    t.curpos = 0;
    t.escseen = 0;
    t.keywords = NULL;
    t.keyword_count = 0;
    t.line = NULL;
    t.prefix = strdup(prefix);
    t.run = 1;
    t.size = 0;
    t.c_lflag_bak = 0;
    t.allocated_size = 0;
    t.tabcount = 0;
    t.tabline = NULL;
    t.tabpos = 0;
    t.history = NULL;
    t.history_pointer = 0;
    t.history_size = 0;
    return t;
}

static void cell_keyword_ins(CellKeyword **cks, siz *keyword_count, CellKeyword ck){
    siz i = cell_keyword_get_pos(ck.keyword, *cks, *keyword_count);

    if(i < *keyword_count && strcmp((*cks)[i].keyword, ck.keyword) == 0){
        free(ck.keyword);
        (*cks)[i].color = ck.color;
        (*cks)[i].action = ck.action;
        (*cks)[i].shorthelp = ck.shorthelp;
        return;
    }

    *cks = (CellKeyword *)realloc(*cks, sizeof(CellKeyword) * ++(*keyword_count));
    for(siz k = *keyword_count - 1;k > i;k--){
        (*cks)[k] = (*cks)[k - 1];
    }
    (*cks)[i] = ck;
}

void cell_insert_keyword(Cell *t, CellKeyword ck){
    cell_keyword_ins(&t->keywords, &t->keyword_count, ck);
}

void cell_add_keyword(Cell *t, const char *keyword, const char *help, cell_action action){
    cell_keyword_ins(&t->keywords, &t->keyword_count, cell_create_keyword(keyword, help, action));
}

static void cell_keyword_destroy(CellKeyword ck){
    for(siz i = 0;i < ck.subcommand_count;i++)
        cell_keyword_destroy(ck.subcommands[i]);
    if(ck.subcommands)
        free(ck.subcommands);
    free(ck.keyword);
}

void cell_destroy(Cell *t){
    for(siz i = 0;i < t->keyword_count;i++){
        cell_keyword_destroy(t->keywords[i]);
    }
    free(t->keywords);
    free(t->prefix);
    if(t->tabline)
        free(t->tabline);
    for(siz i = 0;i < t->history_size;i++)
        free(t->history[i]);
    free(t->history);
}

void cell_default_help(const CellStringParts csp, Cell *t){
    (void)csp;
    siz part = 1;
    CellKeyword *keywords = t->keywords, *last_match = NULL;
    siz keyword_count = t->keyword_count;
    char *partstr = NULL;
    while(part < csp.part_count){
        if(csp.parts[part][0] != '\0'){ 
            CellKeyword *key = cell_keyword_match(csp.parts[part], keywords, keyword_count);
            if(key != NULL){
                partstr = csp.parts[part];
                last_match = key;
            }
            if(key != NULL && key->subcommands != NULL){
                keywords = key->subcommands;
                keyword_count = key->subcommand_count;
            }
            else
                break;    
        }
        part++;
    }
    u8 print = 1;
    if(partstr != NULL){
        printf(ANSI_COLOR_GREEN "\n[Help] " ANSI_COLOR_RESET ANSI_FONT_BOLD "%s" ANSI_COLOR_RESET, partstr);
        printf("\n%s", last_match->longhelp == NULL ? last_match->shorthelp 
                : last_match->longhelp);
        if(last_match->subcommands == NULL){
            print = 0;
        }
        else
            cell_pinfo("%" Psiz " subcommand(s) available", keyword_count);
    }
    if(print){
        if(keyword_count > 0){
            printf( ANSI_FONT_BOLD "\nNo. Command   \t\tDescription" ANSI_COLOR_RESET);
            printf("\n=== ==========\t\t===========");
            for(siz i = 0;i < keyword_count;i++){
                printf( "\n%-3zu " ANSI_FONT_BOLD "%-10s" ANSI_COLOR_RESET "\t\t%s", (i+1), keywords[i].keyword, keywords[i].shorthelp);
            }
        }
    }
}

void cell_default_not_implemented(const CellStringParts parts, Cell *t){
    (void)t;
    cell_pwarn("Action not implemented : %s", parts.parts[0]);
}

CellKeyword cell_create_keyword(const char *keyword, const char *help, cell_action action){
    CellKeyword ck;
    ck.action = action;
    ck.color = CELL_COLOR_GREEN;
    ck.shorthelp = help;
    ck.keyword = strdup(keyword);
    ck.length = strlen(keyword);
    ck.subcommands = NULL;
    ck.subcommand_count = 0;
    ck.longhelp = NULL;
    return ck;
}

void cell_add_subkeyword(CellKeyword *parent, CellKeyword sub){
    cell_keyword_ins(&parent->subcommands, &parent->subcommand_count, sub);
}

#ifdef CELL_DEBUG
#define setname(name) \
    void cell_d##name(const char *msg, ...) {
#else
#define setname(name) \
        void cell_p##name(const char *msg, ...) {
#endif

#define display(name, color, text) \
            setname(name) \
            printf(ANSI_FONT_BOLD); \
            printf(ANSI_COLOR_##color "\n" text " "); \
            printf(ANSI_COLOR_RESET); \
            va_list args; \
            va_start(args, msg); \
            vprintf(msg, args); \
            va_end(args); \
        }

        display(dbg, GREEN, "[Debug]")
        display(info, BLUE, "[Info]")
        display(err, RED, "[Error]")
        display(warn, YELLOW, "[Warning]")


#define print(name, color) \
            void cell_p##name(const char* msg, ...){ \
                printf(ANSI_COLOR_##color); \
                va_list args; \
                va_start(args, msg); \
                vprintf(msg, args); \
                va_end(args); \
                printf(ANSI_COLOR_RESET); \
            }

            print(red, RED)
            print(blue, BLUE)
            print(grn, GREEN)
            print(ylw, YELLOW)
            print(cyn, CYAN)
            print(mgn, MAGENTA)
