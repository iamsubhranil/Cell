#include <string.h>
#include <stdio.h>
#include "cell.h"

static void exit_action(const CellStringParts parts, Cell *t){
    (void)parts;
    cell_pinfo("Exiting..\n");
    t->run = 0;
}

static void sub_action(const CellStringParts parts, Cell *t){
    (void)t;
    cell_pinfo("Running subcommand %s", parts.parts[0]);
    cell_pdbg("Argument Count : %" "zd", parts.part_count);
    for(size_t i = 0;i < parts.part_count;i++){
        cell_pdbg("Argument[%" "zd" "] : %s", i+1, parts.parts[i]);
    }
}

static void prompt_action(const CellStringParts csp, Cell *c){
    if(csp.part_count == 1){
        cell_perr("Give a string to set as prompt!");
        return;
    }
    
    size_t total_size = 0, j = 0;
    for(size_t i = 1;i < csp.part_count;i++)
        total_size += strlen(csp.parts[i]) + 1;

    char prompt[total_size + 1];
    for(size_t i = 1;i < csp.part_count;i++){
        size_t k = 0;
        if(i > 1)
            prompt[j++] = ' ';
        while(csp.parts[i][k] != '\0')
            prompt[j++] = csp.parts[i][k++];
    }
    prompt[j] = '\0';
    free(c->prefix);
    c->prefix = strdup(prompt);
}

typedef struct{
    const char *keyword;
    const char *help;
    cell_action action;
} MyKeyword;

static void hi_test_action(const CellStringParts parts, Cell *t){
    (void)t;
    cell_pdbg("In hi_test_action!");
    cell_pdbg("Argument Count : %" "zd", parts.part_count);
    for(size_t i = 0;i < parts.part_count;i++){
        cell_pdbg("Argument[%" "zd" "] : %s", i+1, parts.parts[i]);
    }
}

static void keyword_action(const CellStringParts parts, Cell *t){
    (void)t;
    cell_pinfo("Running keyword : %s", parts.parts[0]);
    cell_pdbg("Argument Count : %" "zd", parts.part_count);
    for(size_t i = 0;i < parts.part_count;i++){
        cell_pdbg("Argument[%" "zd" "] : %s", i+1, parts.parts[i]);
    }
}

static MyKeyword keywords[] = {
    {"hi", "Just say hi", hi_test_action},
    {"run", "Run a program", keyword_action},
    {"hemp", "Sort test", keyword_action},
    {"run_from", "Run  program from given location", keyword_action},
    {"load", "Load a program in memory", keyword_action},
    {"load_at", "Load a program in memory at a given location", keyword_action},
    {"save", "Save the program", keyword_action},
    {"save_as", "Save the program as a given filename", keyword_action},
    {"interpret", "Start a REPL", keyword_action},
    {"exit", "Exit from the shell", exit_action},
    {"prompt", "Set a new prompt", prompt_action},
    {"pronpt", "Sort test", keyword_action}
};

void nested_term_action(const CellStringParts csp, Cell *t1);

Cell init_cell(const char *pref){
    Cell t = cell_init(pref);
    for(size_t i = 0;i < sizeof(keywords)/sizeof(MyKeyword);i++){
        CellKeyword ck = cell_create_keyword(keywords[i].keyword,
                keywords[i].help, keywords[i].action);
        ck.longhelp = "Sample Long Help";
        for(size_t j = 0;j < sizeof(keywords)/sizeof(MyKeyword);j++){
            if(i == 0 || i == j)
                continue;
            cell_add_subkeyword(&ck, cell_create_keyword(keywords[j].keyword,
                        keywords[j].help, j == 0 ? hi_test_action :
                keywords[j].action != exit_action ? sub_action : exit_action));
        }
        cell_insert_keyword(&t, ck);
    }
    cell_add_keyword(&t, "help",  "Show this help", &cell_default_help);
    cell_add_keyword(&t, "nest",  "Nest another shell", nested_term_action);

    return t;
}

void nested_term_action(const CellStringParts parts, Cell *t1){
    (void)parts; (void)t1;
    cell_pdbg("Spawning nested cell");
    char np[strlen(t1->prefix) + strlen(ANSI_FONT_BOLD) + strlen(ANSI_COLOR_RESET) + 4];
    snprintf(np, sizeof(np), "%s" ANSI_FONT_BOLD "->>" ANSI_COLOR_RESET, t1->prefix);
    Cell t = init_cell(np);
    cell_repl(&t);
    cell_destroy(&t);
}

int main(){
    Cell t = init_cell(ANSI_FONT_BOLD ">>" ANSI_COLOR_RESET);
    cell_repl(&t);
    cell_destroy(&t);
    return 0;
}
