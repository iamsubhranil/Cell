Changelog
=========

2.0.1 (2018-09-03)
------------------

#### Fix
- Don't always show subcommands when `help <keyword>` is invoked. [Subhranil Mukherjee]

2.0.0 (2018-09-03)
------------------

#### New
- Descriptive keyword help when `help <keyword>` is invoked. [Subhranil Mukherjee]

#### Change
- Automatically free argument `CellStringParts` when the callback returns. [Subhranil Mukherjee]
- Keyword color now defaults to green. [Subhranil Mukherjee]
- Pass unknown subcommand to the parent keyword as argument. [Subhranil Mukherjee]

1.3.2 (2018-08-26)
------------------

#### Fix
- Don't bail out from search until the search boundary is exhausted. [Subhranil Mukherjee]

1.3.1 (2018-06-11)
------------------

#### Fix
- Don't leak va_args. [Subhranil Mukherjee]


1.3 (2018-06-11)
----------------

#### New
- Integrated some cosmetic display functions right into the core
  library. [Subhranil Mukherjee]
- Implemented subcommand system. [Subhranil Mukherjee]


1.2.1 (2018-06-10)
------------------

#### Fix
- Doc fixes. [Subhranil Mukherjee]

1.2 (2018-06-10)
----------------

#### New
- Implemented command history, triggered by up/down arrows. [Subhranil
  Mukherjee]

#### Changes
- Cell: renamed all private functions to match the cell namespace.
  [Subhranil Mukherjee]


1.1 (2018-06-09)
----------------
- Added changelog. [Subhranil Mukherjee]
- Merge branch 'feature/autocomplete' into develop. [Subhranil
  Mukherjee]
- Autocomplete : enable contexual autocompletion Now, pressing tab more
  than once results in generation of next possible matching keyword.
  [Subhranil Mukherjee]
- Updated readme. [Subhranil Mukherjee]
- Cell : free all string parts after autocompletion try. [Subhranil
  Mukherjee]
- Cell : don't try to autocomplete if all is space in the line.
  [Subhranil Mukherjee]
- Cell : treat tab as 4 spaces. [Subhranil Mukherjee]
- Autocomplete : initial implementation. [Subhranil Mukherjee]

1.0 (2018-06-08)
----------------
- Cell:default_help : left align keywords by default. [Subhranil
  Mukherjee]
- Cell:keyword_get_pos : use search bound while searching for a keyword
- Driver : fixes. [Subhranil Mukherjee]
- Updated gitignore. [Subhranil Mukherjee]
- Cell : added keyword_get_pos and used thoroughly to reduce complexity
  Now, all methods use keyword_get_pos, which is basically the keyword
  position finder algorithm that was previously inside cell_add_keyword.
  Due to its less complexity, all functions which searches for a keyword
  uses this method. [Subhranil Mukherjee]
- Cell : automatically ascending sort keywords while registering.
  [Subhranil Mukherjee]
- Driver : added prompt keyword to change the prompt. [Subhranil
  Mukherjee]
- Cell : dynamically allocate all strings to avoid invalid pointer
  access. [Subhranil Mukherjee]
- Added gitignore. [Subhranil Mukherjee]
- Cell : few improvements 1. Hightlight only the first word 2.
  Hightlight non-keywords with red 3. Skip all the preceding spaces and
  treat the first word as command. [Subhranil Mukherjee]
- Cell : added default help and not_implemented functions. [Subhranil
  Mukherjee]
- Keywords : added help description. [Subhranil Mukherjee]
- Major refactoring, renamed everything to reflect the Cell namespace.
  [Subhranil Mukherjee]
- Term : check whether the keyword exists already before adding it to
  the array. [Subhranil Mukherjee]
- Driver : free parts when done. [Subhranil Mukherjee]
- Term : maintain allocated size separately to avoid shrinking.
  [Subhranil Mukherjee]
- Create README.md. [Subhranil Mukherjee]
- Add LICENSE. [iamsubhranil]
- Initial commit. [Subhranil Mukherjee]


