# Cell
## An embeddable shell for POSIX systems, wrtiten in C

This library is meant to be an easily embeddable shell for providing a customized CUI to your program. 
It supports syntax highlighting, intrinsics, tab-triggered autocompletion, command history and subcommands.

## API

#### Files

1. `cell.h` : The library header containing declarations of the structures and functions.

2. `cell.c` : The implementation of the library.

3. `main.c` : A driver showing the basic usage of the library.

#### Types

1. `CellStringParts` : To tokenize a string at the occurrence of a space and store the parts. The `parts` array contains the null terminated splitted strings, and the `part_count` variable contains the number of parts in the string. Any consecutively occurring space is considered to be a string of size 0 (1 if you count the null character).

2. `cell_action` : The method type which will be invoked when a keyword is entered. The first argument to the function is the whole line given at the shell at the time of invokation, space splitted, including the keyword name as the first part. The second argument is the shell itself, whose internal state can also be modified by the action, for example setting the `run` variable to `0` will cause the REPL to exit when the action method terminates.

2. `CellKeyword` : To specify a collection of characters as a keyword. The `keyword` variable is the actual keyword string, `color` variable is 
one of the `CELL_COLOR_*` from `cell.h`, and the `action` variable is a pointer to the method which is to be invoked when a line preceding that 
keyword is entered in the shell, followed by an `Enter`. The `subcommands` and `subcommand_count` is the container and counter for
all subcommands of the keyword, respectively. `shorthelp` carries the oneliner help string which is shown on the default invokation of `help`. `longhelp` optionally carries a more descriptive help message for the keyword when `help <keyword>` is invoked. If `longhelp` is `NULL` (which is the default), then the oneliner `shorthelp` is displayed instead.

3. `Cell` : The actual shell structure, which contains the state of the shell at any given point of time.

#### Functions

1. `cell_init` : Initialize a shell with a given prefix string, i.e. the leftmost string to display when the shell is waiting for user input.

2. `cell_add_keyword` : Add a keyword to the shell, with an one-liner help message to describe what it does, and an action to invoke when the keyword is entered.

3. `cell_repl` : Start the REPL loop. The only way to exit this loop is by setting the `run` variable to 0, so you must provide some sort of 
an `exit` keyword to the user.

4. `cell_destroy` : Destroy a initialized cell. It basically frees the keyword array, history and the prefix, since the cell itself is 
statically allocated.

5. `cell_stringparts_free` : Free a `CellStringParts` structure.

6. `cell_create_keyword` : Create a `CellKeyword` structure by passing arguments. If you add it to a `Cell`, the `Cell` will automatically
manage its memory for you.

7. `cell_add_subkeyword` : Add a subkeyword to a keyword. The subkeyword will have its own action, although it will not be highlighted. 
See the commit [`307dcce`](https://gitlab.com/iamsubhranil/Cell/commit/307dcce0d106dd9c9f6babeafe7b6cad8db805c1) for more details.

8. `cell_insert_keyword` : Insert a `CellKeyword` structure directly into a `Cell`. This and `cell_add_keyword` has the same function, except, 
if you want a keyword to have subcommands, you have to use `cell_create_keyword` and `cell_add_subkeyword`, and then finally do 
`cell_insert keyword` to insert the parent keyword into the `Cell`.

9. `cell_p*` : Print various types of colored messages to the user. See the header for more details.
